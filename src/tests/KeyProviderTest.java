package tests;


import static org.junit.Assert.assertNotEquals;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.gsd.encryption.EncryptAES;

import org.gsd.encryption.EncryptRSA;
import org.junit.jupiter.api.Test;



import static org.hamcrest.CoreMatchers.instanceOf;

import org.gsd.encryption.key.KeyProvider;

import java.util.Base64;

import javax.crypto.spec.SecretKeySpec;

class KeyProviderTest {

	@Test
	void testRSA() throws UnsupportedEncodingException {
		KeyProvider keyProvider = new KeyProvider();
		EncryptRSA encrypt = new EncryptRSA();
		byte[] key = keyProvider.getBinaryKey(new String(Base64.getDecoder().decode("LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUE0bTBjckVMTFd3c1VxWkVRUnh6QQp3MDcxd3NONFVxTTFmbEx5dzhUVlliWURVNW1MVVhLdmpUUDRRY3JoS255TjZ1R3k2QThiNEw0ZzBvT1NVT2FNClhLYVU4Tnh0ZkNrbHM2R21ML3k3RWJQMTltQVJEMUE2d3d3UGs0bTE1Z1MvRTd2a3dUbDJOZ252ayt0amloUnUKb0pPOUFNK3lvNjg0YkgzMXRVeG9rNU92MU9GMmJaUzJUbEpkaDV4NnN0ZHNweGdpN0Q0NHFqakpuOW9UYS9jaAp6MmJFN1czS1IzcE1TSlc5Syt3UzA0L3IyYkI4RHBTU2I3VWpYVWZUdVY2M2xBNnIzbktMM2QxeEwvQkc1clhUClNoTkNTeGsybmR2OWFVREhRN29EM0o2ZnNycExaOFpFRU9MRjNYaW94TDFoQU5SY3Y3dk9xQm1oNTNIdjkvWWEKdXdJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg=="), "UTF-8"));
		
		byte[] privateKey = keyProvider.getBinaryKey(new String(Base64.getDecoder().decode("LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2UUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktjd2dnU2pBZ0VBQW9JQkFRRGliUnlzUXN0YkN4U3AKa1JCSEhNRERUdlhDdzNoU296VitVdkxEeE5WaHRnTlRtWXRSY3ErTk0vaEJ5dUVxZkkzcTRiTG9EeHZndmlEUwpnNUpRNW94Y3BwVHczRzE4S1NXem9hWXYvTHNScy9YMllCRVBVRHJEREErVGliWG1CTDhUdStUQk9YWTJDZStUCjYyT0tGRzZnazcwQXo3S2pyemhzZmZXMVRHaVRrNi9VNFhadGxMWk9VbDJIbkhxeTEyeW5HQ0xzUGppcU9NbWYKMmhOcjl5SFBac1R0YmNwSGVreElsYjByN0JMVGordlpzSHdPbEpKdnRTTmRSOU81WHJlVURxdmVjb3ZkM1hFdgo4RWJtdGROS0UwSkxHVGFkMi8xcFFNZER1Z1BjbnAreXVrdG54a1FRNHNYZGVLakV2V0VBMUZ5L3U4Nm9HYUhuCmNlLzM5aHE3QWdNQkFBRUNnZ0VCQU5nY0drelQ1N0g2em5yVmFQcm1sNWtTQ1FFZWZmNHBwa251czZSa2Z1RW0KenVBQTRmY1lCalE0VVEvQ3FzeFpXejYrc25wSXlXdmxMQUNnOFlyanNUNVJCRUVyc2hmYVVoQzBEYUVwa0FiaQpnN1RtbGdIT3FuYzFVTHlacUpKbEgxYWZkRU4wSzc2Q0Z6ZFFhSTRHMkZ2TkJvejZyOThJdzByUm9pMnJRdmp1CkplcWJmem01TStoRnNKRHA0N09LR2lIcTZ1Ti91TlFKK3piSW84RmNiSlhSNG0wdFBpbFI5YUhXM29ldmFMVVQKeXM2cXlLUHM3K1FsenNvQStudUhLeFJIaWg3WTZRWHhkK2FlNCthSG9Zcjd3WFZZZUxMTGFKczlnZVU4cmlzSwphWTZSTGdTZ0huZDFDYnFxSDBJalVBcit0T3MxVEE3enduYWxQK1B5c3NrQ2dZRUEra25zN2ZxamJOUytJNWJECm5kdTJnK0NiK2pqWW5mSlRlWndZa2ZVeU02Qm9rbDgvMGJPczI4OXppTm5EdCt4MG44NWZpYjlhbXR3c3Vyd3cKai9rVE0yMGtxYVk0RXNvMENYa3BwY2RqVzBHVmcrdUVEYmZQVEtDa3M3MGluakhWSFhqYURXSmdjOFpTU1hDOApiTFhLZDE0VitzY0Z0cE84Qk11c2EzMWg0Y1VDZ1lFQTU1Zkt6UU11dUNlbVVoWEU3TDhyd1pSd2tzNHRRRTNrCmZTaGRoaDhUT1NUTDFRcThxUVNRSDVEeVhpdVNnVHVOY3pOdUZzOGJnSWhocnlpcVVPZ3dCT3o4OXRnMHp5cWcKczEvbTM1SU94WnBhTlppamd6WFUyazRia2Yxd25RbXltWUZWdWxZaEJBRDJ5aHpqV2tIUFpFWnVQNjcrVU84TApvdEgzTjNFZVVuOENnWUVBc2ZIUkV6ajQ2NjB0S1p4TW8zVE1CR0t5UW1vY0I5OTdYYUNQRDJabU5MSTg1cVAxCmVaL21xN2lJQ2FuWkxuQmNVaW1sWHdlKzRRN2NkajdKdi9KenBrSllxSXpXblBWSm5kcVlzNVpZbTA2N2dEajMKZjlGSS9zL1BpVUpVMUdmVmZzaW5McERjeGpNZjJicmZHK0w3ZVhaWTVoeU5nVXJ6aCsxT1BuVzhHNGtDZ1lFQQpsZkxaTXk1cFZJRVBsanhVR2JtWm1pZ0RUM3BCSUU2T3IxNndWMFBOallVK21MenY5RHRwR1pjZTVrd3QrcERWCmdBNS9yRE1DcXBFak5tY05CMytlZjhIMTg5bC9MOWQ0MzBzcnovdmdwcDdPNDc3bHQvSGwxRWNZWU1rSTNoVEcKVjgrKzh2d0xPbTFwWE9taFp2SVBWdXluTzRLUlZWUEJZL1d3ckd3SVp2OENmMzhCQUNzSVRGWUhkMFpTYUZZdwpVRnU0cXFQR0FrU1cvSTV0WTg0Vm5jMEVoOGlJN1o0ME5pRzVpbFE0NGlhQVVKQ1lXNXE1c3dGTm1Wa2U0TGpqCkxURkNmYzNQZy9hY2cybnNmUENoUTlWQ0poLzBuQ0tZNEFyV2ZVcmVYZXFFbDdKUXVtOERzYlVjeGR2UzlqYzYKSHVCM0RpQ2l2d3ROZXRsUnc3UXdIOU09Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0K"), "UTF-8"));
		
		try {
			RSAPublicKey pubkey = keyProvider.getPublicKey(key);
			RSAPrivateKey privKey = keyProvider.getPrivateKey(privateKey);
			assertThat(pubkey, instanceOf(RSAPublicKey.class));
			byte[] data = Base64.getDecoder().decode("j0eQHXZqad0xpuoPRX136z8BMOvzi1Vt3e0Z7vXEvKLP3fk23JUSWhmytWLvvKH0bkxplkEORev827Z2EQiPnbYda3bWA7x1m9HnmRZkMSuHhKa86Ush4kLYsneVL12PkTEuHLDjBBx+sb75YA5Gsze25e8kugSV8KZsjbr7SqIdfdG9ML1N37G7Opv1RouFa4z2TXo9mV9mTP0b2Eph7W9hgezECgTSG+J5SI0Q+VGSEgfH05gQdDaoY1V+Z4Cd1NPyE03L8QqUuPVrclnPPLWPVPLLa8dkI1RuomoBNUPsXDKjGuK5Mnuv0UiqUObjwVxN2sGrwh4jEJGRYJ/dMt2bPB4nLsU81eVtn+UfWZYBwNCYR83g04Aq7TilvxTmZaiVjr1oLG2u7VTsiLAfi3T5HtKMtCMB4fp7N0XrsK17cHtqfTt3tguLyuTNkmRVkPqDJunU5W4m4xd0ZE9sb8ajjOEGOBvfO/mX7sdEUvWE9iplFxsKLUd8IBiWqO1fR08/IP5U/m6qt1fuMjHSJY7aOAPyz1Pk0RaBeCwshxR/FpCOdJwCEdnTKwHLrXB24ibwx0yq5f5A/Sm+JM7ouyb7wKzTsGpGB1Bee7iQ0GMEkvfOEz6AWqZXvKvRHJGGM4OWL9kM97IFBIc1sN1isJ3zJCb7x8wsjLWPsUaNi4jOvHx5lsbkh6gbXycXTspNsXLyNaSIrkqFVuw/BkSZSIrQ0GISqHY0gObMUhC6yO/CqO2p+I9tw8ovGu0lAdjyxQon4S+1LOIk6+EqdspAnuCc1DO+3rN0rE658NdK/7iZv2nLAIra7SU9UDWwwPQp64BtwujHDOn+Mwlgg5Knz2Lm/055E3o6LpyH5fNzQXAocKU+ZSc1jIig58JDYIkTSUIiKWcrrAGujwZl8G3iN2eb+1RwI2RlHwZARlgYq5xGm3gfM49IrLeu0oDCL/pMLVGSyeoCH6n7Yh2AWXkwJVRlNssGKI+hVfSsZcFi3RDzMijRPmTnGm8LHcRvvrrNaXJ69/lD7SoPjrSGX4tAs4H5H+UnmJYQ0bw/XVE7xYnfjNOnYU1rM6E+vZHQs0/5RLZZFHGuxq5JYuvR4SKQ7jPDolfYuNwkDy7J+daWmWvt1JnrmDO4fr0xc2g8RPId3gelpnOoAMPIImFWlCTFSGESFgGj6YBnwQaaX7GyUjC191b3UKr/jtlf5SI8pyFUFps6fA619UOtdl3mYSY/91q+/C8rJ1NfBY+csHjM4jjdYvQz9yWcjsjw6AVKnIln1KnF9PUXUqkJSwz84JpoZTkSCeayBsQFtzqp8exJvRgQ6MVQdgfXRiq6Xk1YBl6eZ4VNNhNxsG9QXo0d3RXb55xo0rRPoa/MwtCAS7vQMWA3NqJL7UjEmeQdVkA7zoiQ9jb7FAzWVW3ApU0kOuxRqkjhukNpZ2S6RgTdnAifTi8bh1FP+WXfAZcfvQ4uhFsDTFNDaFUYAAETxn2EidyDzo4oFJKTnEqqaRvgK4MEMmKomlzLdXcMYjN6+5sHybrMwCSDqtLCuW749oIdHhC/La76sPTeT7D6yP932jmoJ9I7cL6WVbF2foBfGCK2cgtiK0bcmZ7g3Xd4XTfgUiHAOFAo73CO7NsEOBwbzF1eBnIrNC/IfAeTl58s/W/j9F+TS3ayy99t4tfCKu5amoIFI9mR4ueLEaVNvnI6LUDbMI0=");
			String encryptedData = encrypt.decryptChunks(privKey, data);
			assertNotEquals("Hello world", new String(encryptedData));
		} catch (Exception e) {
			System.out.print(e.getMessage());
			fail("exception was thrown");
		}				
	}
	
	@Test
	void testCounter() {
		EncryptAES encrypt = new EncryptAES();
		byte[] expectedCounter = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
		byte[] result = encrypt.getCounter(1);
		assertArrayEquals(expectedCounter, result);
	}
	 
	@Test
	void testAES() throws UnsupportedEncodingException {
		EncryptAES encrypt = new EncryptAES();
		SecretKeySpec key = encrypt.base64ToKey("XMOLwop2LlAzZQjDuBcBwpPCgsOIP8Klw4V2wqPChBvCjiXDtsO1c8O/w4nDoMOUXw==");
		String text = "Lorem ipsum";
		
		try {
			byte[] encrypted = encrypt.encrypt(key, text.getBytes());
			assertNotEquals(new String(encrypted, "UTF-8"), text);
			assertNotEquals(encrypted, text.getBytes());
			String decrypted = new String(encrypt.decrypt(key, encrypted), "UTF-8");
			assertEquals(text, decrypted);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			fail("exception was thrown");
		}		
	}
	
	@Test
	void testSymmetricKeyExtract() {
		String response = "{\\\"status\\\":{\\\"internalStatus\\\":\\\"0\\\",\\\"statusMessage\\\":\\\"ok\\\"},\\\"data\\\":{\\\"sessionId\\\":\\\"CD4DDiayg12gAXgGPGaJCxboxkwk47XyY9JyJ939a9yg7Bch9OJGWDIM4oCuKIH2iMq7iac546WSNXZNYf3Uq7WJOZmQi6y7g+aaIlO/UIaoNr708N4t2C9dSo7Imxd/2selatrwRdfqffmaJHrywfr0s3ZELEv/IRkYxm5FVxyPtrkugGcP/z1Ojxky5m3KJm7c9Gbh7Wt9Svs00IPyGgJq5ePYOl4tJ/96LxVAXiG//UbIfMqH4ovq+eCUoEOPpZvkY9N2M1HbAUAw/IGQ9hOfEcifeoMdhQx5D8ZSjVWAojKjufoLLAfbZfR6cGDm/8PYugC5uuFUwGUz9KO27Yg==\\\",\\\"user\\\":{\\\"~ClassName\\\":\\\"User\\\",\\\"~StoreTime\\\":\\\"20180606T103808+02:00\\\",\\\"Name\\\":\\\"GSDWebService\\\",\\\"test\\\":{\\\"~ClassName\\\":\\\"Mitarbeiter\\\",\\\"~ObjectID\\\":\\\"6NB9\\\",\\\"~StoreTime\\\":\\\"20180123T154738+02:00\\\",\\\"Name1\\\":\\\"webservice\\\",\\\"Name2\\\":\\\"\\\",\\\"Name3\\\":\\\"\\\"}},\"symmetricKey\":\"L8Ovw57CojxFCcOMwpscXDrCqcOBw58qw7tSH8Kyw7kSF8KgWULDujxaw4lNHg==\"}}";
		KeyProvider keyProvider = new KeyProvider();
		
		String key = keyProvider.getKeyFromResponse(response, "symmetricKey");
		
		assertEquals("L8Ovw57CojxFCcOMwpscXDrCqcOBw58qw7tSH8Kyw7kSF8KgWULDujxaw4lNHg==", key);
	
		
	}
	
	@Test
	void testSymmetricKeyExtractFail() {
		String response = "{\"status\":{\"internalStatus\":\"0\",\"statusMessage\":\"ok\"},\"data\":{\"sessionId\":\"CD4DDiayg12gAXgGPGaJCxboxkwk47XyY9JyJ939a9yg7Bch9OJGWDIM4oCuKIH2iMq7iac546WSNXZNYf3Uq7WJOZmQi6y7g+aaIlO/UIaoNr708N4t2C9dSo7Imxd/2selatrwRdfqffmaJHrywfr0s3ZELEv/IRkYxm5FVxyPtrkugGcP/z1Ojxky5m3KJm7c9Gbh7Wt9Svs00IPyGgJq5ePYOl4tJ/96LxVAXiG//UbIfMqH4ovq+eCUoEOPpZvkY9N2M1HbAUAw/IGQ9hOfEcifeoMdhQx5D8ZSjVWAojKjufoLLAfbZfR6cGDm/8PYugC5uuFUwGUz9KO27Yg==\",\"user\":{\"~ClassName\":\"User\",\"~StoreTime\":\"20180606T103808+02:00\",\"Name\":\"GSDWebService\",\"test\":{\"~ClassName\":\"Mitarbeiter\",\"~ObjectID\":\"6NB9\",\"~StoreTime\":\"20180123T154738+02:00\",\"Name1\":\"webservice\",\"Name2\":\"\",\"Name3\":\"\"}},\"symmetricKey\":\"L8Ovw57CojxFCcOMwpscXDrCqcOBw58qw7tSH8Kyw7kSF8KgWULDujxaw4lNHg==\"}}";
		KeyProvider keyProvider = new KeyProvider();
				
		InvalidParameterException exception = assertThrows(InvalidParameterException.class, () -> {
			keyProvider.getKeyFromResponse(response, "non-existing-key");
        });
		
	}
	

}
