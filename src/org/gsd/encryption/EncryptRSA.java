package org.gsd.encryption;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.security.interfaces.RSAPrivateKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class EncryptRSA {
	protected int base = 256;
	
	public EncryptRSA() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}
		
	public byte[] encrypt(RSAPublicKey key, byte[] plainText) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException
	{
		Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA1AndMGF1Padding", "BC"); 
	    cipher.init(Cipher.ENCRYPT_MODE, key);  
	    return cipher.doFinal(plainText);
	}
	
	public String encryptString(RSAPublicKey key, byte[] plainText) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		int chunks = plainText.length / base;
		Boolean dividable = plainText.length % base == 0;
		chunks = dividable ? chunks : chunks + 1;
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < chunks; i++) {
			int nextMax = (i+1) * base;
			int max = nextMax > plainText.length ? plainText.length : nextMax; 
			byte[] chunkArray = Arrays.copyOfRange(plainText, i * base, max);
			builder.append(new String(encrypt(key, chunkArray), "UTF-8"));
		}
		return builder.toString();
		
	}
	
	public String decryptChunks(RSAPrivateKey key, byte[] encryptedText) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		int chunks = encryptedText.length / base;
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < chunks; i++) {
			byte[] chunkArray = Arrays.copyOfRange(encryptedText, i * base, (i+1) * base);
			builder.append(new String(decrypt(key, chunkArray), "UTF-8"));
		}
		return builder.toString();
		
	}
	
	public byte[] decrypt(RSAPrivateKey key, byte[] encryptedText) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA1AndMGF1Padding", "BC"); 
	    cipher.init(Cipher.DECRYPT_MODE, key);  
	    return cipher.doFinal(encryptedText);
	}
}
