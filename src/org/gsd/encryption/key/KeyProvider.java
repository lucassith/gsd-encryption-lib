package org.gsd.encryption.key;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidParameterException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class KeyProvider {
	public byte[] getBinaryKey(String pemKey) {
		
		String newLineSeparator = pemKey.contains("\r\n") ? "\\r\\n" : "\\n";
		List<String> lines = new ArrayList<>(Arrays.asList(pemKey.split(newLineSeparator)));
		if (lines.size() < 2) 
		    throw new IllegalArgumentException("Insufficient input");
		if (!lines.remove(0).startsWith("--"))
		    throw new IllegalArgumentException("Expected header");
		if (!lines.remove(lines.size() - 1).startsWith("--")) 
		    throw new IllegalArgumentException("Expected footer");
		return Base64.getDecoder().decode(String.join("", lines));
	}
	
	public RSAPublicKey getPublicKey(byte[] key) throws IOException, GeneralSecurityException {     
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(key));
        return pubKey;
    }
    
	public RSAPrivateKey getPrivateKey(byte[] key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(new PKCS8EncodedKeySpec(key));
        return privKey; 
	}
	
	public String getKeyFromResponse(String response, String keyName) throws InvalidParameterException {
		String search = new String("\"" + keyName + "\":\"");
		int indexOfFirst = response.indexOf(search);
		if (indexOfFirst < 0) {
			throw new InvalidParameterException("No key " + keyName + "in response: " + response);
		}
		String symmetricKey = response.substring(response.indexOf(search)+search.length());
		return symmetricKey.substring(0, symmetricKey.indexOf("\""));
	}
}
