package org.gsd.encryption;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptAES {
	public EncryptAES() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}

	public SecretKeySpec base64ToKey(String base64) throws UnsupportedEncodingException {
		String symmetric = new String(Base64.getDecoder().decode(base64), "UTF-8");
		byte[] key = new byte[symmetric.length()];
		for (int i = 0; i < symmetric.length(); i++) {
			key[i] = (byte) symmetric.charAt(i);
		}
		return new SecretKeySpec(key, "AES");
	}

	public byte[] base64ToUIntByte(String base64) {
		return Base64.getDecoder().decode(base64);
	}

	public byte[] getCounter(int counterValue) {
		byte[] iv = new byte[16];
		for (int index = 15; index >= 0; --index) {
			iv[index] = (byte) (counterValue % 256);
			counterValue = (int) (counterValue / 256);
		}
		return iv;
	}

	public byte[] encrypt(SecretKeySpec key, byte[] plainText)
			throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		byte[] iv = getCounter(1);
		IvParameterSpec ivSpec = new IvParameterSpec(iv);

		Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
		cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
		byte[] encrypted = cipher.doFinal(plainText);
		return encrypted;
	}

	public byte[] decrypt(SecretKeySpec key, byte[] plainText) throws InvalidKeyException,
			InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		byte[] iv = getCounter(1);
		Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		return cipher.doFinal(plainText);

	}
}
